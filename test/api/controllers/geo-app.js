var should = require('should');
var request = require('supertest');
var server = require('../../../app');

describe('controllers', function() {

  describe('get', function() {

    describe('GET /geolocation-randomizer/59.2/10.1/10/10', function() {

      it('should return one default geolocation', function(done) {

        request(server)
          .get('/geolocation-randomizer/42/42/0/1')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function(err, res) {
            should.not.exist(err);

            res.body.should.eql([ { lat : 42, lng : 42 } ]);

            done();
          });
      });

        it('should return 4 default geolocations', function(done) {

            request(server)
                .get('/geolocation-randomizer/42/42/0/4')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function(err, res) {
                    should.not.exist(err);

                    res.body.should.eql([
                        { lat : 42, lng : 42 },
                        { lat : 42, lng : 42 },
                        { lat : 42, lng : 42 },
                        { lat : 42, lng : 42 }
                    ]);

                    done();
                });
        });

    });

  });

});
