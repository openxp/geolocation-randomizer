var geo = require('./geo.js');

module.exports = {
    get: get
};

function get(req, res) {
    var lat = req.swagger.params.lat.value;
    var long = req.swagger.params.long.value;
    var radius = req.swagger.params.radius.value;
    var count = req.swagger.params.count.value;

    res.json(geo.generate({
        'lat': lat,
        'lng': long
    }, radius, count));
}
