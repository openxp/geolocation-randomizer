# geolocation-randomizer swagger api
Thanks to and https://github.com/ljl for creating https://github.com/ljl/geolocation-randomizer
build on the original https://gist.github.com/mkhatib/5641004 by https://gist.github.com/mkhatib 
on which this service is built.

##Production api
https://api.opdata.org/geolocation-randomizer/42/42/42/42

##Documentation uri
https://swaggerhub.com/api/opdata/geolocation-randomizer/1.0.0